package sample;

import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by saria on 4/9/18.
 */
public class SpeechController implements Initializable {

    @FXML
    ImageView recognizeButton;
    @FXML
    Button pauseButton;
    @FXML
    TextArea resultArea;
    @FXML
    TextArea streetsName;
    FadeTransition ft;
    Sphinx4Recognizer sphinx4Recognizer = new Sphinx4Recognizer();
    private static boolean recognizerStarted;

    public void beginRecognition() {
        if (ft != null) {
            ft.play();
        } else {
            startFadeEffect();

        }
        pauseButton.setVisible(true);
        if (!recognizerStarted) {
            sphinx4Recognizer.recognizeLiveSpeech();
        } else {
            sphinx4Recognizer.setIgnoreRecognitionResult(false);
        }
        recognizerStarted = true;
    }

    private void startFadeEffect() {
        ft = new FadeTransition(Duration.millis(1000), recognizeButton);
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setCycleCount(Timeline.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();
    }

    public void pauseRecognition() {
        pauseButton.setVisible(false);
        ft.stop();
        sphinx4Recognizer.setIgnoreRecognitionResult(true);
    }

    public void clearArea() {
        resultArea.clear();
    }

    private void readStreetNames() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader bf = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/streets.txt")));
            String text;
            while ((text = bf.readLine()) != null) {
                stringBuilder.append(text + '\n');
            }
            streetsName.setText(stringBuilder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        readStreetNames();
        sphinx4Recognizer.recognitionResultProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                resultArea.appendText(t1 + " \n");
            }
        });
        // resultArea.textProperty().bind(Bindings.createStringBinding(() -> resultArea.getText() + "\n" + sphinx4Recognizer.recognitionResultProperty().get(), sphinx4Recognizer.recognitionResultProperty()));
    }
}
