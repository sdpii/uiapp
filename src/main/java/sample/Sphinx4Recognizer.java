package sample;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by saria on 4/5/18.
 * <p>
 * Reference: https://github.com/goxr3plus/Java-Speech-Recognizer-Tutorial--Calculator/blob/master/!!New%20Specific%20Tutorial%20For%20JavaFX!!/src/application/SpeechRecognizer.java
 */
public class Sphinx4Recognizer {
    private final String AcousticModelPath = "resource:/aze/aze.cd_cont_200";
    private final String DictionaryPath = "resource:/aze/aze.dic";
    private final String LanguageModelPath = "resource:/aze/aze.lm";
    private Configuration configuration;

    private LiveSpeechRecognizer recognizer;
    private SimpleBooleanProperty speechRecognizerIsRunning = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty ignoreRecognitionResult = new SimpleBooleanProperty(false);
    private StringProperty recognitionResult = new SimpleStringProperty("");

    ExecutorService executorService = Executors.newFixedThreadPool(2);


    public StringProperty recognitionResultProperty() {
        return recognitionResult;
    }

    public void setRecognitionResult(String recognitionResult) {
        this.recognitionResult.set(recognitionResult);
    }

    public Sphinx4Recognizer() {

        configuration = new Configuration();
        configuration.setAcousticModelPath(AcousticModelPath);
        configuration.setDictionaryPath(DictionaryPath);
        configuration.setLanguageModelPath(LanguageModelPath);
        try {
            recognizer = new LiveSpeechRecognizer(configuration);
        } catch (Exception e) {
            e.printStackTrace();
        }
       //checkResources();
    }

    public void recognizeLiveSpeech() {
        if (speechRecognizerIsRunning.get()) {
            System.out.println("Speech recognition is already running");

        } else {
            executorService.submit(() -> {
                Platform.runLater(() -> {
                    speechRecognizerIsRunning.set(true);
                    ignoreRecognitionResult.set(false);
                });
                recognizer.startRecognition(true);
                try {
                    while (speechRecognizerIsRunning.get()) {
                        System.out.println("thread running");
                        SpeechResult speechResult = recognizer.getResult();
                        if (!ignoreRecognitionResult.get()) {
                            if (speechResult == null) {
                                System.out.println("nothing recognized");
                            } else {
                                System.out.println(speechResult.getHypothesis());
                                recognitionResult.set(speechResult.getHypothesis());
                            }
                        }
                    }
                    System.out.println("thread not running");
                } catch (Exception e) {
                    e.printStackTrace();
                    Platform.runLater(() -> {
                        speechRecognizerIsRunning.set(false);
                    });
                }
            });
        }
    }

    public synchronized void setIgnoreRecognitionResult(Boolean res) {
        Platform.runLater(() -> {
            ignoreRecognitionResult.set(res);
        });
    }

//    public void checkResources() {
//
//        executorService.submit(() -> {
//            try {
//                while (true) {
//                    if (!AudioSystem.isLineSupported(Port.Info.SPEAKER)) {
//                        System.out.println("Mic is not supported");
//                    } else {
//                        System.out.println("Mic is supported");
//                    }
//
//                    Thread.sleep(500);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//    }


}
