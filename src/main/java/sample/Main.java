package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/speechRecognizerUI.fxml"));
        primaryStage.setTitle("Speech Recognition System");
        primaryStage.setScene(new Scene(root, 1200, 600));
        primaryStage.show();
    }


    public static void main(String[] args) {
       // Sphinx4Recognizer  sphinx4Recognizer = new Sphinx4Recognizer();
       // sphinx4Recognizer.recognizeLiveSpeech();
       launch(args);
    }
}
